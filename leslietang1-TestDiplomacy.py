from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_solve,diplomacy_eval,diplomacy_read,diplomacy_print

class TestDiplomacy(TestCase):

    # reading
    def test_read_1(self):
        s = "A Madrid Hold"
        i,j,k = diplomacy_read(s)
        self.assertEqual(i, "A")
        self.assertEqual(j, "Madrid")
        self.assertEqual(k, "Hold")
        
    def test_read_2(self):
        s = "B Turkeytown Support A"
        i,j,k = diplomacy_read(s)
        self.assertEqual(i, "B")
        self.assertEqual(j, "Turkeytown")
        self.assertEqual(k, ["Support", "A"])
        
    # eval    
    def test_eval_1(self):
        v = diplomacy_eval([["A","Madrid","Hold"],["B","Barcelona",["Move","Madrid"]],["C","London",["Move", "Madrid"]]])
        self.assertEqual(v,["A [dead]","B [dead]","C [dead]"])

    def test_eval_2(self):
        v = diplomacy_eval([["A","Madrid","Hold"],["B","Barcelona",["Move", "Madrid"]],["C","London",["Move","Madrid"]],["D","Paris",["Support","B"]],["E","Austin",["Support", "A"]]])
        self.assertEqual(v,["A [dead]","B [dead]","C [dead]","D Paris","E Austin"])

    def test_eval_3(self):
        v = diplomacy_eval([["A", "Madrid", "Hold"]])
        self.assertEqual(v,["A Madrid"])

    def test_eval_4(self):
        v = diplomacy_eval([["A","Madrid","Hold"],["B", "Turkeytown",["Support","A"]],["C","Potato",["Move","Turkeytown"]]])
        self.assertEqual(v,["A Madrid","B [dead]","C [dead]"])
        
    def test_eval_5(self):
        v = diplomacy_eval([["A","Madrid","Hold"],["B", "Turkeytown",["Move","Madrid"]],["C","Potato",["Move","Madrid"]]])
        self.assertEqual(v,["A [dead]","B [dead]","C [dead]"])

    def test_eval_6(self):
        v = diplomacy_eval([["A","Madrid","Hold"],["B", "Turkeytown",["Support","A"]],["C","Potato",["Move","Madrid"]],
                            ["D","Frisco",["Support","C"]],["E","Plano",["Move","Paris"]],["F","Austin",["Support","E"]],["G","Paris","Hold"]])
        self.assertEqual(v,["A [dead]","B Turkeytown","C [dead]","D Frisco","E Paris","F Austin","G [dead]"])
         
    #print
    def test_print(self):
        w = StringIO()
        diplomacy_print(w, ["A Madrid"])
        self.assertEqual(w.getvalue(), "A Madrid\n")
        
    #solve
    def test_solve(self):
        r = StringIO("A Madrid Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")

 
if __name__ == "__main__":
    main()
    
""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
